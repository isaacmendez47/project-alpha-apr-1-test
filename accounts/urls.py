# feature 7.4.0:
# importing the view(create_login) so we can render it
# importing path from django
from django.urls import path

# feature 9.2.1:
# importing the view 'user_logout'
from accounts.views import user_login, user_logout, user_signup

# feature 7.4:
# registered the view(create_login) to a path
# feature: 9.2.2:
# registered the view(user_logout) for a url
# (why are the instructions hella detailed here)
urlpatterns = [
    path("signup/", user_signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("login/", user_login, name="login"),
]
