from django.contrib import admin

# feature 4.0: impport the models to register from models.py
from projects.models import Project

# Register your models here.


# feature 4: import the models to register
# with the app. this makes it so we can see it in the
# Django admin site
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "description",
        "owner",
        "id",
    )
