# feature 14.3.0:
# importing redirect
from django.shortcuts import render, get_object_or_404, redirect

# feature 5.0:
# import the model 'Project' so we can make a view
from projects.models import Project

# feature 8.1.0:
# importing login required decorator
from django.contrib.auth.decorators import login_required

# feature 14.1.0:
# importing projectform
from projects.forms import ProjectForm

# Create your views here.


# feature 5.1 projects list(mainpage)
# feature 8.1 protecting the list view so only those who
# log in can view it
@login_required
def show_project(request):
    # feature 8.2: changing the query set to 'filter(owner=request.user)' so
    # the logged in user can see their own tasks
    project_lists = Project.objects.filter(owner=request.user)
    context = {
        "project_lists": project_lists,
    }
    return render(request, "projects/list.html", context)


# feature 13.1-2: created a view and protected it
@login_required
def project_details(request, id):
    project_detail = get_object_or_404(Project, id=id)
    context = {
        "project_details": project_detail,
    }
    return render(request, "projects/detail.html", context)


# feature 14.1-2: creates a new project object
# and is protected
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            # feature 14.3: redirects to projects list if successful
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
