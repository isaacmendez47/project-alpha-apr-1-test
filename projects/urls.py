# feature 5.2.0: made a new urls.py file in the app
# we also import the view(show_project) function
# we want the browser to display.
# we also import the path from django.

# feature 13.3.0: imported the view(project_details)
# feature 14.4.0: imported the view(create_project)
from projects.views import show_project, project_details, create_project
from django.urls import path

# feature 5.2: registering the view(show_project)
# feature 13.3: registering the view(project_details)
# feature 14.4: registering the view(create_project)
urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>/", project_details, name="show_project"),
    path("", show_project, name="list_projects"),
]
