# feature 14.0.0:
# creating a model that allows us to view. we import the
# model that we want to use(Project) and ModelForm
from projects.models import Project
from django.forms import ModelForm


# feature 14.0.1:
# creating a modelform for Project
# so users can create a new project
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
