# feature 15.0: imported task model(Task)
# and ModelForm
from tasks.models import Task
from django.forms import ModelForm


# feature 15.1.0 creating TaskForm model in preperation
# for view import
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
