# feature 15.5.0: created file, imported view(create_task)
# and django urls path
# feature 16.3.0 imported view(show_tasks)
from tasks.views import create_task, show_tasks
from django.urls import path

# feature 15.5: registered view(create_tasks)
# feature 16.3: registered view(show_tasks)
urlpatterns = [
    path("mine/", show_tasks, name="show_my_tasks"),
    path("create/", create_task, name="create_task"),
]
