from django.contrib import admin

# feature 12.0: importing model to register
from tasks.models import Task

# Register your models here.


# i wonder if the tests test for whats inside the registered model
# apparently it doesnt. i can leave this as a blank field?
# not that i wont but thats interesting
# feature 12: this
@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
        "id",
    )
